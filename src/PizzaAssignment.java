import java.util.Scanner;

public class PizzaAssignment {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        boolean done = false;

        int total = 0;

        int toppings = 0;

        System.out.println("Please choose a number from the menu, type q to quit ");
        System.out.println();
        System.out.println("1....Margherita: Tomato, cheese............................... Price: 60 kr." );
        System.out.println("2....Hawaii: Tomato, cheese, ham, pineapple................... Price: 75 kr.");
        System.out.println("3....Italiano: Tomato, cheese, pepperoni...................... Price: 70 kr.");
        System.out.println("4....Meat Lover: Tomato, cheese, pepperoni, ham............... Price: 71 kr.");
        System.out.println("5....Pizza hot: Tomato, cheese, kebab, chili.................. Price: 66 kr.");
        System.out.println("6....Flora: Tomato, ham, beef, pepperoni, cheese.............. Price: 71 kr.");
        System.out.println("7....Viking: Tomato, cheese, beef, onion, chili............... Price: 76 kr. ");
        System.out.println("8....Mama mia: Tomato, cheese, ham, bacon, pepperoni.......... Price: 66 kr.");
        System.out.println("9....Lambada: Chicken, tomato, cheese, pineapple, bell pepper. Price: 66 kr.");
        System.out.println("10...Vegetarian: Tomato, cheese, mushroom, onion, olive....... Price: 59 kr. ");


        String textInput = in.next();


        if (textInput.equals("q")) {
            done = true;
        } else {
            System.out.println("You chose item " + textInput);
            System.out.println();
        }


        if (textInput.equals("1")) {
            total = total + 60;
        } else if (textInput.equals("2")) {
            total = total + 75;
        } else if (textInput.equals("3")) {
            total = total + 70;
        } else if (textInput.equals("4")) {
            total = total + 71;
        } else if (textInput.equals("5")) {
            total = total + 66;
        } else if (textInput.equals("6")) {
            total = total + 71;
        } else if (textInput.equals("7")) {
            total = total + 76;
        } else if (textInput.equals("8")) {
            total = total + 66;
        } else if (textInput.equals("9")) {
            total = total + 66;
        } else if (textInput.equals("10")) {
            total = total + 59;
        }

        System.out.println("Please choose a topping from the menu, type q for no toppings ");
        System.out.println("");
        System.out.println("1.....Extra cheese..... 5kr. ");
        System.out.println("2.....Mushrooms....... 8 kr. ");
        System.out.println("3.....Tomatoes........ 7 kr. ");
        System.out.println("4.....Sausage........ 10 kr. ");
        System.out.println("5.....Potatoes........ 9 kr. ");
        System.out.println("6.....Garlic.......... 8 kr. ");
        System.out.println("7.....Green Peppers....7 kr. ");
        System.out.println("8.....Spinach......... 8 kr. ");
        System.out.println("9.....Onions......... 10 kr. ");
        System.out.println("10... Black Olives.... 8 kr. ");


        String textInput2 = in.next();

        if (textInput2.equals("q")) {
            done = true;
        } else {
            System.out.println("You chose topping " + textInput2);
            System.out.println();
        }

        if (textInput2.equals("1")) {
            toppings = toppings + 5;
        } else if (textInput2.equals("2")) {
            toppings = toppings + 8;
        } else if (textInput2.equals("3")) {
            toppings = toppings + 7;
        } else if (textInput2.equals("4")) {
            toppings = toppings + 10;
        } else if (textInput2.equals("5")) {
            toppings = toppings + 9;
        } else if (textInput2.equals("6")) {
            toppings = toppings + 8;
        } else if (textInput2.equals("7")) {
            toppings = toppings + 7;
        } else if (textInput2.equals("8")) {
            toppings = toppings + 8;
        } else if (textInput2.equals("9")) {
            toppings = toppings + 10;
        } else if (textInput2.equals("10")) {
            toppings = toppings + 8;
        }


        System.out.println("Please choose the size of your pizza ");
        System.out.println();
        System.out.println("1...Children size ");
        System.out.println("2...Standard size ");
        System.out.println("3...Family size   ");

        String textInput3 = in.next();

        if (textInput3.equals("1")) {
            total = (int) (total * 0.75);
        } else if (textInput3.equals("2")) {
            total = (int) (total * 1.0);
        } else if (textInput3.equals("3")) {
            total = (int) (total * 1.50);
        }


        System.out.println("The total cost is " + (total + toppings));

    }
}


